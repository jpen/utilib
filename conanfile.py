from conans import ConanFile, CMake, tools
import os


class UtilibConan(ConanFile):
    name = "utilib"
    version = "0.0.1"
    license = "Boost"
    url = "https://gitlab.com/l.j.persson/utilib"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"

    def source(self):
       self.run("git clone git@gitlab.com:l.j.persson/utilib.git")

    def build(self):
        cmake = CMake(self.settings)
        shared = "-DBUILD_SHARED_LIBS=ON" if self.options.shared else ""
        self.run('cmake %s/utilib %s %s' % (self.conanfile_directory, cmake.command_line, shared))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="utilib/include")
        self.copy("*.hpp", dst="include", src="utilib/include")
        self.copy("*utilib.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["utilib"]
