#include <iostream>
#include "OnScopeExit.h"

int main() {
    //hello();
    std::cout<<"*** Running example, will fail by default, implement yours! ***\n";
    return 0; // fail by default, remember to implement your test
}
